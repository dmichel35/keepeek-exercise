package com.keepeek.exercise;

public class KeepeekExercise {

	/**
	 * Encloses terms with bold HTML tag.
	 * @param input : a String that contains the given terms.
	 * @param termToEncloseList : the list of terms to enclose.
	 * @return the input String containing the enclosed terms.
	 */
	public static String encloseWithBoldHtml(final String input, final String[] termToEncloseList) {
		
		String result = input;
		
		if (input != null && termToEncloseList != null) {
			for(final String termToEnclose : termToEncloseList) {	
				/*
				 * (?iu) : Unicode case insensitive matching.
				 * (?<!\\S) : Negative lookbehind which asserts that the match won't be preceded by a non-space character.
				 * (?!\\S) : Negative lookahead which asserts that the match won't be followed by a non-space character.
				 */
				result = result.replaceAll("(?<!\\S)((?iu)"+termToEnclose+")(?!\\S)", "<b>$1</b>");
			}
		}
		
		return result;
	}
	
}
