package com.keepeek.exercise;

import org.junit.Assert;
import org.junit.Test;

public class KeepeekExerciseEvolutionTest {

	@Test
	public void testEvolution() {

		 final String input = "class est le mot clé qui permet de définir une classe";
	
		 final String result = KeepeekExercise.encloseWithBoldHtml(input, new String[] { "class", "classe" });
	
		 Assert.assertEquals("<b>class</b> est le mot clé qui permet de définir une <b>classe</b>", result);
	}
	
	@Test
	public void testEvolutionLookbehind() {
		
		final String input = "class - et non lass - est le mot clé qui permet de définir une classe";
		
		final String result = KeepeekExercise.encloseWithBoldHtml(input, new String[] { "lass", "classe" });
	
		Assert.assertEquals("class - et non <b>lass</b> - est le mot clé qui permet de définir une <b>classe</b>", result);
	}
	
	@Test
	public void testEvolutionWithPoint() {
		
		final String input = ".class est le mot clé qui permet de définir une classe";
		
		final String result = KeepeekExercise.encloseWithBoldHtml(input, new String[] { "class", "classe" });

		Assert.assertEquals(".class est le mot clé qui permet de définir une <b>classe</b>", result);
	}
	
}
