package com.keepeek.exercise;

import org.junit.Assert;
import org.junit.Test;

public class KeepeekExerciseTest {

	private static final String[] TERMS_TO_ENCLOSE = {"keepeek", "java"};
	
	@Test
	public void testBasic() {
		
		final String input = "keepeek est un éditeur java";
		
		final String result = KeepeekExercise.encloseWithBoldHtml(input, TERMS_TO_ENCLOSE);
		
		Assert.assertEquals("<b>keepeek</b> est un éditeur <b>java</b>", result);
	}
	
	@Test
	public void testWithUpperCase() {
		
		final String input = "keepEEk est un éditeur java";
		
		final String result = KeepeekExercise.encloseWithBoldHtml(input, TERMS_TO_ENCLOSE);
		
		Assert.assertEquals("<b>keepEEk</b> est un éditeur <b>java</b>", result);
	}
	
	@Test
	public void testWithUpperCase2() {
		
		final String input = "keePeek est un éditeur JAVA";
		
		final String result = KeepeekExercise.encloseWithBoldHtml(input, TERMS_TO_ENCLOSE);
		
		Assert.assertEquals("<b>keePeek</b> est un éditeur <b>JAVA</b>", result);
	}
	
	@Test
	public void testWithMultipleInstances() {
		
		final String input = "keePeek keepEEk est un éditeur JAVA";
		
		final String result = KeepeekExercise.encloseWithBoldHtml(input, TERMS_TO_ENCLOSE);
		
		Assert.assertEquals("<b>keePeek</b> <b>keepEEk</b> est un éditeur <b>JAVA</b>", result);
	}
	
	@Test
	public void testWithEmptyString() {
		
		final String result = KeepeekExercise.encloseWithBoldHtml("", TERMS_TO_ENCLOSE);
		
		Assert.assertEquals("", result);
	}
	
	@Test
	public void testWithNullString() {
		
		final String result = KeepeekExercise.encloseWithBoldHtml(null, TERMS_TO_ENCLOSE);
		
		Assert.assertEquals(null, result);
	}
	
	@Test
	public void testWithEmptyTermsToEnclose() {
		
		final String input = "keepeek est un éditeur java";
		
		final String result = KeepeekExercise.encloseWithBoldHtml(input, new String[]{});
		
		Assert.assertEquals(input, result);
	}
	
	@Test
	public void testWithNullTermsToEnclose() {
		
		final String input = "keepeek est un éditeur java";
		
		final String result = KeepeekExercise.encloseWithBoldHtml(input, null);
		
		Assert.assertEquals(input, result);
	}
	
	@Test
	public void testWithAccentInUpperCase() {
		
		final String input = "keepeek est un Éditeur java";
		
		final String result = KeepeekExercise.encloseWithBoldHtml(input, new String[] { "éditeur" });
		
		Assert.assertEquals("keepeek est un <b>Éditeur</b> java", result);
	}
	
}
